<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MacPain_AssignProductsStockData',
    __DIR__
);
